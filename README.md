[RustBook.ir](http://rustbook.ir)

---

سورس اصلی پروژه: [Programming Rust](https://gitlab.com/mtarif/programming-rust)

---

به دلیل مشکلی که هنور علتش رو نمی دونم، پروژه در [Programming Rust](https://gitlab.com/mtarif/programming-rust) با موفقیت build نشد و به صورت موقت فایل های استاتیک پروژه ساخته و به اینجا ارسال میشن و روت اصلی [RustBook.ir](http://rustbook.ir) تا حل مشکل، به اینجا منتقل میشه.
